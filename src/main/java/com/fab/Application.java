package com.fab;

import com.fab.services.BatchService;
import com.fab.utils.HibernateUtils;
import org.hibernate.Session;


public class Application {

    public static void main(String[] args) {

        Session session = HibernateUtils.getSessionFactory().openSession();

        BatchService service = new BatchService(session);
        service.insertPermission();

        session.close();
    }
}
