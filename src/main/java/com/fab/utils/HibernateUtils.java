package com.fab.utils;

import com.fab.entities.AccountType;
import com.fab.entities.Permission;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import java.util.EnumSet;

/**
 * Created by Firas on 8/29/2017.
 */
public class HibernateUtils {

    private static SessionFactory SESSION_FACTORY = null;

    private static void buildSessionFactory(boolean export) {
        try {
            // Create the ServiceRegistry from hibernate.cfg.xml
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySetting(AvailableSettings.SHOW_SQL, "true")
                    .configure("hibernate.cfg.xml")
                    .build();

            // Create a metadata sources using the specified service registry.
            Metadata metadata = new MetadataSources(serviceRegistry)
                    .addAnnotatedClass(AccountType.class)
                    .addAnnotatedClass(Permission.class)
                    .buildMetadata();

            if (export) {
                EnumSet<TargetType> enumSet = EnumSet.of(TargetType.SCRIPT);
                SchemaExport schemaExport = new SchemaExport();
                schemaExport.setFormat(true);
                schemaExport.setDelimiter(";");
                schemaExport.setOutputFile("db-schema.sql");
                schemaExport.execute(enumSet, SchemaExport.Action.BOTH, metadata);
            }

            SESSION_FACTORY = metadata.buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory(){
        if(SESSION_FACTORY == null){
            buildSessionFactory(true);
        }
        return SESSION_FACTORY;
    }
}
