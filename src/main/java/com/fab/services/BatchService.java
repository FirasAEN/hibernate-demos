package com.fab.services;

import com.fab.entities.AccountType;
import com.fab.entities.Permission;
import lombok.AllArgsConstructor;
import org.hibernate.Session;

import java.util.Date;

@AllArgsConstructor
public class BatchService {
    private Session session;

    public void insertAccount() {
        session.beginTransaction();

        AccountType type = new AccountType();
        type.setName("Check");
        type.setCreatedDate(new Date());
        type.setLastUpdatedDate(new Date());
        type.setCreatedBy("Firas ABED EL NABI");
        type.setLastUpdatedBy(new Date().toString());
        session.persist(type);
        session.getTransaction().commit();
    }

    public void insertPermission() {
        session.beginTransaction();

        Permission p = new Permission();
        p.setCode("READ");

        session.persist(p);
        session.getTransaction().commit();
    }
}
