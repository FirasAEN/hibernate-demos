package com.fab.services;

import com.fab.entities.AccountType;
import org.h2.tools.Server;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.jupiter.api.*;

import java.util.Date;

import static java.lang.Thread.sleep;


public class BatchServiceTest {

    private static final long SLEEP_TIME = 3600000;
    private final boolean shouldWait = false;
    private static SessionFactory sessionFactory = null;
    private static Session session = null;

    @BeforeAll
    static void setup() {
        try {
            Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();
            StandardServiceRegistry standardRegistry
                    = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml")
                    .build();

            Metadata metadata = new MetadataSources(standardRegistry)
                    .addAnnotatedClass(AccountType.class)
                    .getMetadataBuilder()
                    .build();

            sessionFactory = metadata
                    .getSessionFactoryBuilder().build();
            session = sessionFactory.openSession();

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    @AfterAll
    static void tear(){
        sessionFactory.close();
    }

    @BeforeEach
    void setupThis(){
        session.beginTransaction();
    }

    @AfterEach
    void tearThis() throws InterruptedException {
        session.getTransaction().commit();
        if (shouldWait) {
            sleep(SLEEP_TIME);
        }
    }

    @Test()
    public void createAccountType() {
        AccountType type = new AccountType();
        type.setName("Check");
        type.setCreatedDate(new Date());
        type.setLastUpdatedDate(new Date());
        type.setCreatedBy("Firas ABED EL NABI");
        type.setLastUpdatedBy(new Date().toString());

        Assertions.assertNull(type.getAccountTypeId());
        session.persist(type);
        Assertions.assertNotNull(type.getAccountTypeId());
        Assertions.assertEquals(1, type.getAccountTypeId());
    }
}
