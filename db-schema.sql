
    drop table if exists account_type;

    drop table if exists permission;

    create table account_type (
       ACCOUNT_TYPE_ID bigint not null auto_increment,
        LAST_UPDATED_DATE datetime(6),
        CREATED_BY varchar(255),
        CREATED_DATE datetime(6),
        LAST_UPDATED_BY varchar(255),
        NAME varchar(255),
        primary key (ACCOUNT_TYPE_ID)
    ) engine=InnoDB;

    create table permission (
       PERMISSION_ID bigint not null auto_increment,
        LABEL varchar(255),
        primary key (PERMISSION_ID)
    ) engine=InnoDB;
